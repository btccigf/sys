#!/usr/bin/perl
use strict;
use IO::Socket; # libio-interface-perl
use IO::Interface qw(:flags);
use POSIX qw(strftime);

# arp-scan must be installed
my $bin = "/usr/bin/arp-scan";
die "Cannot run $bin, exit" unless -x $bin;

# work on every interface one by one
my $s = IO::Socket::INET->new(Proto => 'udp');
my @interfaces = $s->if_list;
for my $if (@interfaces) {
    my $flags = $s->if_flags($if);

    # skip loopback
    next if $flags & IFF_LOOPBACK;

    print "--------- interface ".$s->if_addr($if)." ---------\n";

    # skip if not running
    print "not running\n" and next unless $flags & IFF_RUNNING;
    # skip if no arp (like VPN)
    print "no ARP. VPN?\n" and next if $flags & IFF_NOARP;
    
    # slurp data
    my %scan;
    open(ARPSCAN, "$bin -l --interface=$if |");
    while(<ARPSCAN>) {
	chomp();
	
	# interesting lines start by IPv4
	my $ip = $1 if /^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s/;
	next unless $ip;

	# try to find host name (non FQDN)
	my $name = `/usr/bin/dig -x $ip +short`;
	$name = substr($name, 0, index($name, '.'));

	# extract mac address
	my $mac = $1 if /\s([a-fA-F0-9:]{17}|[a-fA-F0-9]{12})\s/;

	# keep extra info
	my $extrainfo = $1 if /^$ip\s$mac\s(.*)$/;
	
	@{$scan{$ip}} = ($ip,$name,$mac,$extrainfo);
	
    }
    close(ARPSCAN);

    # display (unsophisticated) sorted data
    foreach my $ip (sort(keys %scan)) {
	# give some room for hostname
	printf("%s\t%-22s %s %s\n", @{$scan{$ip}})
    }

    print "\n\n";
}

print strftime "(%c)\n", localtime;

# EOF

