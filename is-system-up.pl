#!/usr/bin/perl
use strict;
use Fcntl qw(:flock);
use Sys::Syslog;
use Capture::Tiny qw(capture); 
use Sys::Hostname;
    
# debian packages:
#    libio-interface-perl libcapture-tiny-perl

# bin paths
my $bin_ifup = "/sbin/ifup";
my $bin_ifdown = "/sbin/ifdown";
my $bin_rcservice = "/sbin/rc-service";
    
my $bin_nginx = "/usr/sbin/nginx";
my $bin_samba = "/usr/sbin/nmbd";
my $bin_ssh = "/usr/sbin/sshd";
my $bin_dhcp = "/usr/sbin/dhcpd";
my $bin_pdnss = "/usr/sbin/pdns_server";
my $bin_pdnsr = "/usr/sbin/pdns_recursor";
my $bin_ntopng = "/usr/sbin/ntopng";

###########  functions

# wrapper for system() calls to remove unwanted output garbage
sub systemc {
    my @cmd = @_;
    my ($stdout, $stderr, $exit) = capture { system(@cmd); };
    return $exit;
}

########## RUN

# silently forbid concurrent runs
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

# start logging
openlog("is-system-up.pl", "", "LOG_USER");

# check if network is up on configure and live interfaces
my $daemons_must_restart;
use IO::Interface::Simple;
my @interfaces = IO::Interface::Simple->interfaces;
foreach my $if (@interfaces) {
    # ignore loopback
    next if $if->is_loopback;
    # ignore if not running
    next unless $if->is_running;
    # ignore if an IP is set
    next if $if->address;

    # try to reload the interface
    systemc($bin_ifdown, $if);
    systemc($bin_ifup, $if);

    # log on success (no details, it should be logged also by the DHCP client)
    syslog("LOG_WARNING", "$if reloaded (had no IP assigned)");

    # plan daemons restart if assigned an IP
    $daemons_must_restart++ if $if->address;    
}


# check running daemons
my %daemons;
# check if within a LXC container
my $is_lxc_host = 1;
# check variable "container", should be set running a shell
$is_lxc_host = 0 if $ENV{container};
# check if the sched pid is 1. if not consider we are in a container
# (a bit primitive for should work)
open(SCHED, "/proc/1/sched");
while (<SCHED>) {
	$is_lxc_host = 0 unless m/\(1,/;
	last;
}
close(SCHED);

# go through the list of process
opendir(PROC, "/proc");
while (defined(my $pid = readdir(PROC))) {
    # not a process if not a directory
    next unless -d "/proc/$pid";
    # not a process if not containing cmdline
    next unless -e "/proc/$pid/cmdline";
    # if proc is within a LXC container if contains string /lxc/ in groups
    # skip if running on the host
    my $proc_within_container;
    open(CGROUP, "< /proc/$pid/cgroup");
    while (<CGROUP>) {
	$proc_within_container++ if m/\/lxc\//;
	last if $proc_within_container;
    }
    close(CGROUP);
    next if $proc_within_container and $is_lxc_host;	
    
    # look out for relevant binaries
    open(PID, "< /proc/$pid/cmdline");
    while (<PID>) {
	# check are not sophisticated, worse case we do a useless restart
	$daemons{"nginx"} = 1 if m/$bin_nginx/;
	$daemons{"samba"} = 1 if m/$bin_samba/;
	$daemons{"ssh"} = 1 if m/$bin_ssh/;
	$daemons{"isc-dhcp-server"} = 1 if m/$bin_dhcp/;
	$daemons{"pdns"} = 1 if m/$bin_pdnss/;
	$daemons{"pdns-recursor"} = 1 if m/$bin_pdnsr/;
	$daemons{"ntopng"} = 1 if m/$bin_ntopng/;
	}
    close(PID);
}
closedir(PROC);


if ($daemons_must_restart) {
    # restart found daemons one by one
    my @daemons_to_restart = keys(%daemons);
    syslog("LOG_WARNING", "will restart ".join(",", @daemons_to_restart))
	if scalar(@daemons_to_restart);
    for my $daemon (@daemons_to_restart) {
	systemc($bin_rcservice, $daemon, "restart");
    }
}


# final check, make sure some daemons are up on specific hosts (even if not
# started yet)
my %mandatory_daemons = (
    'prf' => ["isc-dhcp-server", "nginx", "ntopng", "ssh", "pdns", "pdns-recursor"],
    'export' => ["nginx", "ssh", "samba"],
    'import' => ["ssh", "samba"],
    'vrac' => ["nginx"],
    );
for my $host (keys %mandatory_daemons) {
    next unless $host eq hostname();
    foreach my $daemon (@{$mandatory_daemons{$host}}) {
	# ignore if already found to be up
	next if $daemons{$daemon};
	syslog("LOG_WARNING", "$daemon found off, attempt to start it");
	systemc($bin_rcservice, $daemon, "start");	
    }
}
	


# EOF
